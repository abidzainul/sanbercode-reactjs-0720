// Soal 1
console.log("\n---------------------------- SOAL 1 -------------------------------")
const luasLingkaran = (r) => 22/7 * (r*r)
console.log(luasLingkaran(14))
  
const kelilingLingkaran = (d) => 22/7 * d
console.log(kelilingLingkaran(14))

// Soal 2
console.log("\n---------------------------- SOAL 2 -------------------------------")
let kalimat = ""
const addKata = (kata) => {
    kalimat = kalimat.concat(`${kata} `)
}

addKata('saya')
addKata('adalah')
addKata('seorang')
addKata('frontend')
addKata('developer')

console.log(kalimat)

// Soal 3
console.log("\n---------------------------- SOAL 3 -------------------------------")
class Book {
    constructor(name, totalPage, price) {
        this.name = name;
        this.totalPage = totalPage;
        this.price = price;
    }

    showBook = () => `Nama Buku: ${this.name}, Total Page: ${this.totalPage}, Price: ${this.price}`
        
}

class Komik extends Book {
    constructor(name, totalPage, price, isColorfull) {
        super(name, totalPage, price)
        this.isColorfull = isColorfull;
    }

    showKomik = () => `${this.showBook()} Color Full: ${this.isColorfull}`
    
}

const book = new Book("Matematika", 20, 20000)
console.log(book.showBook())

const komik = new Komik("Komik Onepiece", 40, 90000, false)
console.log(komik.showKomik())
