var readBooks = require('./callback.js')
var readBooksPromise = require('./promise.js')
 
// Soal 1
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

var i = 0;
function bacaCallback(time){
    if(i > books.length-1){
        console.log("readBooks DONE\n")
        bacaPromise(10000, 0)
    }else if(time < 2000){
        console.log("readBooks DONE, Waktu tidak cukup untuk membaca\n")
        bacaPromise(10000, 0)
    }else{
        readBooks(time, books[i], bacaCallback)
    }
    i++
}

// Soal 2
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function bacaPromise(time, index) {
    readBooksPromise(time, books[index])
        .then(function (data) {
            index+=1
            if(index > books.length-1)
                bacaPromise(data, 0);
            else
                bacaPromise(data, index);
        })
        .catch(function (error) {
            console.log("Case Error Reject, Waktu: "+error);
        });
}

bacaCallback(10000)