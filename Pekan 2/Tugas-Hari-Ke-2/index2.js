// Soal 2
var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
function baca(time, index) {
    readBooksPromise(time, books[index])
        .then(function (data) {
            index+=1
            if(index > books.length-1)
                baca(data, 0);
            else
                baca(data, index);
        })
        .catch(function (error) {
            console.log("Case Error Reject, Waktu: "+error);
        });
}
 
baca(10000, 0)