// Soal 1
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
var i = 0;
function baca(time){
    if(i > books.length-1){
        console.log("readBooks DONE")
    }else if(time < 2000){
        console.log("readBooks DONE, Waktu tidak cukup untuk membaca")
    }else{
        readBooks(time, books[i], baca)
    }
    i++
}

baca(10000)