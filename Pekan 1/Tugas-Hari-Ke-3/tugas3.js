// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var upper = kataKedua.charAt(0)
kataKedua = upper.toUpperCase().concat(kataKedua.substr(1))
console.log("\n---------------------------- SOAL 1 -------------------------------")
console.log(kataPertama.concat(" ", kataKedua, " ", kataKetiga, " ", kataKeempat.toUpperCase()));

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";
console.log("\n---------------------------- SOAL 2 -------------------------------")
var hasil = parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat)
console.log(`Hasil: ${hasil}`)

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log("\n---------------------------- SOAL 3 -------------------------------")
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai = 40;
console.log("\n---------------------------- SOAL 4 -------------------------------")
if(nilai >= 80){
    console.log("nilai > 80 indeksnya A")
}else if(nilai >= 70 && nilai < 80){
    console.log("nilai > 70 dan nilai < 80 indeksnya B")
}else if(nilai >= 60 && nilai < 70){
    console.log("nilai > 60 dan nilai < 70 indeksnya c")
}else if(nilai >= 50 && nilai < 60){
    console.log("nilai > 50 dan nilai < 60 indeksnya D")
}else if(nilai < 50){
    console.log("nilai < 50 indeksnya E")
}else{
    console.log("Di luar batas kemampuan manusia")
}


// soal 5
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

var month;
switch(bulan) {
    case 1:
        month = "Januari"
        break;
    case 2:
        month = "Februari"
        break;
    case 3:
        month = "Maret"
        break;
    case 4:
        month = "April"
        break;
    case 5:
        month = "Mei"
        break;
    case 6:
        month = "Juni"
        break;
    case 7:
        month = "Juli"
        break;
    case 8:
        month = "Agustus"
        break;
    case 9:
        month = "September"
        break;
    case 10:
        month = "Oktober"
        break;
    case 11:
        month = "November"
        break;
    case 12:
        month = "Desember"
        break;
    default:
        month = "Bulan apakah itu ?"
}
console.log("\n---------------------------- SOAL 5 -------------------------------")
console.log(`${tanggal} ${month} ${tahun} \n`);