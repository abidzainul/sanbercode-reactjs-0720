
// Soal 1
// LOOPING PERTAMA
var index = 2;
console.log("\n----------------------------- SOAL 1 --------------------------------")
console.log("LOOPING PERTAMA")
while(index <= 20) { 
  console.log(index + " - I love coding"); 
  index += 2; 
}

// LOOPING KEDUA
var index = 20;
console.log("\nLOOPING KEDUA")
while(index >= 2) { 
  console.log(index + " - I will become a frontend developer"); 
  index -= 2; 
}

// Soal 2
console.log("\n----------------------------- SOAL 2 --------------------------------")
for(var i = 1; i <= 20; i ++) {
    if(i % 3 == 0 && i % 2 != 0){
        console.log(i + " - I Love Coding"); 
    }else if(i % 2 == 0){
        console.log(i + " - Berkualitas"); 
    }else if(i % 2 != 0){
        console.log(i + " - Santai"); 
    }
}

// Soal 3
console.log("\n---------------------------- SOAL 3 -------------------------------")
var tag = "";
var i = 0;
while (i < 7){
    console.log(tag += "#");
    i++;
};

// Soal 4
console.log("\n----------------------------- SOAL 4 --------------------------------")
var kalimat="saya sangat senang belajar javascript"
console.log(kalimat.split(" "))

// Soal 5
console.log("\n----------------------------- SOAL 5 --------------------------------")
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var sortBuah = daftarBuah.sort()
sortBuah.forEach(val => {
    console.log(val)
});